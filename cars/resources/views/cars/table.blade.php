<table class="table">
	<thead>
		<tr>
			<th>
				VRM
			</th>
			<th>
				Make
			</th>
			<th>
				Model
			</th>
			<th>
				Mileage
			</th>
			<th>
				Features
			</th>
		</tr>
	</thead>
	<tbody>
		@foreach($cars as $car)
			<tr>
				<td>
					{{$car->vrm}}
				</td>
				<td>
					{{$car->manufacturer}}
				</td>
				<td>
					{{$car->model}}
				</td>
				<td>
					{{$car->miles}}
				</td>
				<td>
					@foreach($car->features as $feature)
						{{$feature->name}}
					@endforeach
				</td>
			</tr>
		@endforeach
	</tbody>
</table>