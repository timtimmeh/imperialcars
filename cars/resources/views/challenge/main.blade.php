@extends('layouts/app')

@section('content')
	<form method="POST" action="/challenges">
		@csrf
		<div class="row mt-3">
			<div class="col-4">
				<div class="col-12 p-0">
					<h5>Filter By Manufacturer</h5>
				</div>
				<div class="input-group">
					<input type="hidden" name="challenge" value="3">
					<div class="input-group-prepend">
						<label class="input-group-text" for="manufacturerSelect">Manufacturer</label>
					</div>
					<select class="custom-select" id="manufacturerSelect" name="manufacturer">
						<option @if($selected == false) selected @endif value="">Choose..</option>
						@foreach($manufacturers as $manufacturer)
							<option value="{{$manufacturer->name}}" @if($selected == $manufacturer->name) selected @endif>{{$manufacturer->name}}</option>			
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-4">
				<div class="col-12 p-0">
					<h5>Sort Results</h5>
				</div>
				<div class="custom-control custom-radio pr-2">
					<input type="radio" id="mileageSort" name="sort" class="custom-control-input" value="miles" @if($sort == "miles") checked="checked" @endif>
					<label class="custom-control-label" for="mileageSort">Sort By Mileage</label>
				</div>
				<div class="custom-control custom-radio pr-2">
					<input type="radio" id="makeSort" name="sort" class="custom-control-input" value="manufacturer" @if($sort == "manufacturer") checked="checked" @endif>
					<label class="custom-control-label" for="makeSort">Sort By Make</label>
				</div>
				<div class="custom-control custom-radio pr-2">
					<input type="radio" id="modelSort" name="sort" class="custom-control-input" value="carmodel" @if($sort == "carmodel") checked="checked" @endif>
					<label class="custom-control-label" for="modelSort">Sort By Model</label>
				</div>
				<div class="col-12">
					<div class="alert alert-info">
						<small>I have sorted these DESC so you can see that they do things, otherwise they are already in order!</small>
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="col-12 p-0">
					<h5>Feature Selection</h5>
				</div>
				<div class="custom-control custom-checkbox">
					@foreach($features as $feature)
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" @if(in_array($feature->id, $selectedFeatures)) checked="checked" @endif id="featureCheck{{$feature->id}}" name="features[]" value="{{$feature->id}}">
							<label class="custom-control-label" for="featureCheck{{$feature->id}}">{{$feature->name}}</label>
						</div>
					@endforeach
				</div>
			</div>
		</div>
		<button type="submit" class="btn btn-primary">Update List</button>
	</form>
	<h3>Regular</h3>
	@include('cars/table', ["cars" => $cars])
	<h3 class="mt-5">Cars with Features more than the value of &pound;1500 ({{$expensive->count()}})</h3>
	@include('cars/table', ['cars' => $expensive])
@endsection