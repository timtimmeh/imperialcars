<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
     /**
     * Get the cars for the feature.
     */
    public function cars()
    {
        return $this->belongsToMany("App\Car");
    }

    protected $fillable = ["name", "price"];
}
