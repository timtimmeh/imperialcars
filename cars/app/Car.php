<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    /**
     * Get the model for the car.
     */
    public function carModel()
    {
        return $this->belongsTo('App\CarModel');
    }

    /**
     * Get the features for the car.
     */
    public function features()
    {
        return $this->belongsToMany("App\Feature", "car_feature");
    }

    /**
     * Get the manufacturer for the car.
     */
    public function getManufacturerAttribute()
    {
        return $this->carModel->manufacturer->name;
    }
    /**
     * Get the manufacturer for the car.
     */
    public function getModelAttribute()
    {
    	return $this->carModel->name;
    }

    protected $fillable = ['vrm', 'miles', 'car_model_id'];

    protected $appends = ['manufacturer', 'model'];
}
