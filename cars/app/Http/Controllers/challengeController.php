<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car as Car;
use App\CarModel as CarModel;
use App\Feature as Feature;
use App\Manufacturer as Manufacturer;

class challengeController extends Controller
{
	private $dataForView = array();

	private function getCarsWithExtrasMoreThan(Float $minimumPrice) {
		$expensiveCars = Car::whereHas('features', function($query) use($minimumPrice) {
			$query->havingRaw('SUM(price) > ?', array($minimumPrice));
		})->get();
		$this->dataForView["expensive"] = $expensiveCars;
	}

    public function getStage(int $stage = 0) {
    	switch ($stage) {
    		case 3:
    		default:
    			$stage = 3;
    			$this->getStageThree();
    			break;
    	}
    }

    public function index()
    {
    	$this->dataForView = array(
    		"cars"				=> Car::all(),
    		"features"			=> Feature::all(),
    		"manufacturers"		=> Manufacturer::all(),
    		"selected"			=> false,
    		"selectedFeatures"	=> array(),
    		"sort"				=> false
    	);
    	return $this->outputData();
    }

    private function outputData()
    {
    	$this->getCarsWithExtrasMoreThan(1500);
    	return view('challenge/main', $this->dataForView);
    }

    public function postStage(Request $request) {
    	$stage = $request->challenge;
    	$sort = isset($request->sort) ? $request->sort : "";
    	$selectedFeatures = isset($request->features) ? $request->features : array();
    	switch ($stage) {
    		case 3:
    			$this->searchStageThree($request->manufacturer ?? "", $sort, $selectedFeatures);
    			break;
    	}
    	return $this->outputData($stage);
    }

    private function searchStageThree(String $searchedmanufacturer = "", String $sort = "", Array $selectedFeatures)
    {
    	$cars = array();
    	if(strlen($searchedmanufacturer) > 0) {
    		// $cars = Car::whereHas('carmodel', function($query) use($searchedmanufacturer) {
    		// 	$query->whereHas("manufacturer", function($q2) use($searchedmanufacturer) {
    		// 		$q2->where("manufacturers.name", "=", $searchedmanufacturer);
    		// 	})->get();
    		// })->get();
	    	$manufacturer = Manufacturer::where("name", "=", $searchedmanufacturer)->first();
	    	$models = CarModel::where("manufacturer_id", $manufacturer->id)->get();
	    	$modelIds = array();
	    	foreach ($models as $model) {
	    		$modelIds[] = $model->id;
	    	}
	    	$cars = Car::whereIn("car_model_id", $modelIds)->get();
	    } else {
	    	$cars = Car::all();
	    }
    	if(count($selectedFeatures) > 0) {
			foreach ($cars as $key => $car) {
				$matchedOnFeatures = false;
				foreach ($car->features as $feature) {
					if(in_array($feature->id, $selectedFeatures)) {
						$matchedOnFeatures = true;
					}
				}
				if(!$matchedOnFeatures) {					
					$cars->forget($key);
				}
			}
    	}
    	if(strlen($sort) > 0) {
    		$cars = $cars->sortByDesc($sort);
    	}
    
    	// $cars = Manufacturer::where("name", $manufacturer)->with(["carModels", "carModels.cars"])->get();
    	// $cars = Car::with(["carModel", "carModel.manufacturer"])->where("carModel.manufacturer", "=", $manufacturer)->get();
    	// $cars = Car::with(["carModel", "carModel.manufacturer" => function($query) {global $manufacturer; $query->where("name", "=", $manufacturer);}])->where("carModel.manufacturer", "=", $manufacturer)->get();
    	// var_dump($cars);die();
    	// $cars = Car::with(
    	// 	["carModel", "manufacturer" => function ($query) {$query->where("name", $manufacturer);}]
    	// )->get();
    	// Car::with(['carModel', 'manufacturer'])->where("name", $manufacturer)->get()
		$this->dataForView = array(
    		"cars"          	=> $cars,
    		"features"      	=> Feature::all(),
    		"manufacturers" 	=> Manufacturer::all(),
    		"selected"      	=> $searchedmanufacturer,
    		"selectedFeatures"	=> $selectedFeatures,
    		"sort"          	=> $sort
    	);
    }
}
