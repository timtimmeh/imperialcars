<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
    /**
     * Get the cars for the model.
     */
    public function cars()
    {
        return $this->hasMany('App\Car');
    }

    /**
     * Get the cars for the model.
     */
    public function manufacturer()
    {
        return $this->belongsTo('App\Manufacturer');
    }

    protected $fillable = ['name', 'manufacturer_id', 'car_model_id'];
}
