<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    /**
     * Get the models for the manufacturer.
     */
    public function carModels()
    {
        return $this->hasMany('App\CarModel');
    }

    protected $fillable = ['name'];
}
