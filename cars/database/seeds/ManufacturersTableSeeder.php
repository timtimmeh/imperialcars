<?php

use Illuminate\Database\Seeder;

class ManufacturersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table("cars")->delete();
    	DB::table("car_models")->delete();
        DB::table("manufacturers")->delete();
        DB::table("features")->delete();
    	DB::table("car_feature")->delete();
    	$manufacturers = array(
    		array("name" => "BMW"),
    		array("name" => "Ford"),
    		array("name" => "Vauxhall")
    	);
    	foreach ($manufacturers as $manufacturerInfo) {
    		$newManufacturer = App\Manufacturer::firstOrNew(array('name' => $manufacturerInfo['name']));
			$newManufacturer->save();
    	}

    	$models = array(
    		array("name" => "3 Series", "manufacturer" => "BMW"),
    		array("name" => "5 Series", "manufacturer" => "BMW"),
    		array("name" => "Mondeo",   "manufacturer" => "Ford"),
    		array("name" => "Transit",  "manufacturer" => "Ford"),
    		array("name" => "Zafira",   "manufacturer" => "Vauxhall")
    	);

    	foreach ($models as $modelInfo) {
    		$carModel = new App\CarModel;
    		$carModel->name = $modelInfo['name'];
    		$carModel->manufacturer()->associate(App\Manufacturer::where("name", $modelInfo['manufacturer'])->first());
    		$carModel->save();
    	}

        $features = array(
            array("name" => "Electric Windows", "cost" => 150),
            array("name" => "Bluetooth", "cost" => 250),
            array("name" => "Sat Nav", "cost" => 1100),
            array("name" => "All Wheel Drive", "cost" => 2000),
            array("name" => "Sliding Side Door", "cost" => 1300),
        );

        foreach ($features as $feature) {
            $newFeature = new App\Feature;
            $newFeature->name = $feature['name'];
            $newFeature->price = $feature['cost'];
            $newFeature->save();
        }

    	$cars = array(
    		array("vrm" => "SK14PDF", "miles" => 40013, "model_name" => "3 Series", "model_manufacturer" => "BMW", "features" => array("Electric Windows", "Bluetooth", "Sat Nav")),
    		array("vrm" => "SB17ABC", "miles" => 12293, "model_name" => "3 Series", "model_manufacturer" => "BMW", "features" => array("Electric Windows", "Bluetooth", "Sat Nav", "All Wheel Drive")),
    		array("vrm" => "SA63DEF", "miles" => 66323, "model_name" => "5 Series", "model_manufacturer" => "BMW", "features" => array("Electric Windows", "Bluetooth", "Sat Nav")),
    		array("vrm" => "SB14DFF", "miles" => 44124, "model_name" => "Mondeo",   "model_manufacturer" => "Ford", "features" => array("Electric Windows", "Bluetooth")),
    		array("vrm" => "SB66ABF", "miles" => 88933, "model_name" => "Transit",  "model_manufacturer" => "Ford", "features" => array("Electric Windows", "Bluetooth", "Sliding Side Door")),
    		array("vrm" => "WG12ARB", "miles" => 32033, "model_name" => "Zafira",   "model_manufacturer" => "Vauxhall", "features" => array("Electric Windows")),
    		array("vrm" => "AG12BEF", "miles" => 48742, "model_name" => "Zafira",   "model_manufacturer" => "Vauxhall", "features" => array("Electric Windows"))
    	);

    	foreach ($cars as $carInfo) {
    		$newCar = new App\Car;
    		$newCar->vrm = $carInfo['vrm'];
    		$newCar->miles = $carInfo['miles'];
    		$manufacturerId = App\Manufacturer::where(array("name" => $carInfo['model_manufacturer']))->pluck('id')->first();
            $newCar->carModel()->associate(App\CarModel::where(array("name" => $carInfo['model_name'], "manufacturer_id" => $manufacturerId))->first());
    		$newCar->save();
            foreach ($carInfo['features'] as $carFeature) {
                $feature = App\Feature::where("name", $carFeature)->first();
                $newCar->features()->save($feature);
            }
    	}
    }
}
